package monka.primo.shopping_list.service;

import monka.primo.shopping_list.data.service.UserServiceModel;

public interface UserService {
    void register(UserServiceModel userServiceModel);
    boolean login(UserServiceModel userServiceModel);
}
