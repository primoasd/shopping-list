package monka.primo.shopping_list.service.impl;

import monka.primo.shopping_list.data.entity.User;
import monka.primo.shopping_list.data.service.UserServiceModel;
import monka.primo.shopping_list.repository.UserRepository;
import monka.primo.shopping_list.service.UserService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserServiceImpl implements UserService {
    private final ModelMapper modelMapper;
    private final UserRepository userRepository;

    @Autowired
    public UserServiceImpl(ModelMapper modelMapper, UserRepository userRepository) {
        this.modelMapper = modelMapper;
        this.userRepository = userRepository;
    }

    @Override
    public void register(UserServiceModel userServiceModel) {
        this.userRepository.saveAndFlush(
                this.modelMapper.map(
                        userServiceModel, User.class
                )
        );
    }

    @Override
    public boolean login(UserServiceModel userServiceModel) {
        return this
                .userRepository
                .findByUsernameAndPassword(userServiceModel.getUsername()
                        , userServiceModel.getPassword()) != null;
    }
}
