package monka.primo.shopping_list.data.service;

import javax.validation.constraints.*;

public class UserServiceModel {
    private String username;
    private String email;
    private String password;

    public UserServiceModel() {
    }

    @Size(min = 3, max = 20, message = "Username must be between 3 and 20 characters!")
    @NotNull(message = "The username cannot be null.")
    @NotEmpty(message = "The username cannot be empty.")
    @NotBlank(message = "The username is mandatory.")
    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    @Email(message = "Enter valid email address!")
    @NotNull(message = "The email cannot be null.")
    @NotEmpty(message = "The email cannot be empty.")
    @NotBlank(message = "The email is mandatory.")
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Size(min = 3, max = 20, message = "Password must be between 3 and 20 characters!")
    @NotNull(message = "The password cannot be null.")
    @NotEmpty(message = "The password cannot be empty.")
    @NotBlank(message = "The password is mandatory.")
    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
