package monka.primo.shopping_list.data.binding;

import javax.validation.constraints.*;

public class UserLoginBindingModel {
    private String username;
    private String password;

    public UserLoginBindingModel() {
    }

    @Size(min = 3, max = 20, message = "Username must be between 3 and 20 characters!")
    @NotNull(message = "The username cannot be null.")
    @NotEmpty(message = "The username cannot be empty.")
    @NotBlank(message = "The username is mandatory.")
    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    @Size(min = 3, max = 20, message = "Password must be between 3 and 20 characters!")
    @NotNull(message = "The password cannot be null.")
    @NotEmpty(message = "The password cannot be empty.")
    @NotBlank(message = "The password is mandatory.")
    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
