package monka.primo.shopping_list.data.entity.enumeration;

public enum CategoryName {
    Food, Drink, Household, Other
}
